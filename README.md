# sense-hat

## Installation

`git clone https://gitlab.irstea.fr/raspberry-pi/sense-hat.git`

`sudo apt install libatlas-base-dev python3-sense-hat`

`pip3 install -r sense-hat/requirements.txt`

## Utilisation

`python3 sense-hat/environmental_sensors.py`
