from sense_hat import SenseHat
from datetime import datetime
import time
from pathlib import Path
import csv

sense = SenseHat()

def float_format(f):
    return '{:.2f}'.format(f)

def get_data():
    humidity = float_format(sense.get_humidity())
    temp_humidity = float_format(sense.get_temperature_from_humidity())
    #print("Humidity sensor : {} %rH , {} °C".format(humidity, temp_humidity))

    pressure = float_format(sense.get_pressure())
    temp_pressure = float_format(sense.get_temperature_from_pressure())
    #print("Pressure sensor : {} Millibars , {} °C".format(pressure, temp_pressure))
    
    return [datetime.now().timestamp(), humidity, temp_humidity, pressure, temp_pressure]

# Create folder to store the data
data_folder = Path('./data')
data_folder.mkdir(parents=True, exist_ok=True)
try:
    while(True):
        current_month = datetime.now().month
        file_date = datetime.now().strftime('%Y_%m_%d_%H_%M')
        file_name = data_folder/str('sense_hat_data_' + file_date + '.csv')
        with file_name.open('w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['timestamp', 'humidity', 'temp_humidity', 'pressure', 'temp_pressure'])
            
            while(True):
                writer.writerow(get_data())
                time.sleep(1)
                if(current_month != datetime.now().month):
                    break

except KeyboardInterrupt:
    pass
