#!/bin/bash

# https://askubuntu.com/questions/338937/how-to-use-rsync-via-ssh-with-rsa-key-passphrases
# Utiliser la ligne suivante si vous avez une passphrase sur votre clé ssh
#source $HOME/.keychain/${HOSTNAME}-sh

rsync -av pi@rasp-aiw2d.local:~/sense-hat/data/ $HOME/sense-hat-data > $HOME/rsync.log 2>&1
