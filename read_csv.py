import csv
import matplotlib.pyplot as plt
from scipy import signal
import numpy as np
from datetime import datetime

with open('data/sense_hat_data_2019_03_01_00.csv') as f:
    datas = csv.reader(f)
    header = next(datas)
    temp = []
    time = []
    pressure = []
    for row in datas:
        time.append(datetime.fromtimestamp(float(row[0])))
        temp.append(float(row[2]))
        pressure.append(float(row[3]))

#print(temp)

temp_decimate = signal.decimate(temp, 10, 3, zero_phase=True)
#print(temp_decimate)
temp_mean = np.mean(temp)
print(temp_mean)

temp_filtered = signal.savgol_filter(temp, 53, 3)
pressure_filtered = signal.savgol_filter(pressure, 53, 3)

fig_temp, ax_temp = plt.subplots()
ax_temp.plot(time,temp)
ax_temp.plot(time,temp_filtered)
ax_temp.set_title('Température')
fig_temp.autofmt_xdate()

fig_pres, ax_pres = plt.subplots()
ax_pres.plot(time, pressure)
ax_pres.plot(time, pressure_filtered)
ax_pres.set_title('Pression')
fig_pres.autofmt_xdate()

plt.show()


